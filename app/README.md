# アプリケーション本体

## セットアップ

```
docker run \
  --rm \
  -it \
  -u "node" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app" \
  node \
    npx create-react-app img-viewer --template typescript

docker run \
  --rm \
  -it \
  -u "node" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app/img-viewer" \
  node \
    yarn add @material-ui/core
```

## 動作確認

```
docker run \
  --rm \
  -it \
  -p 3000:3000 \
  -u "node" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app/img-viewer" \
  node \
    yarn start

docker run \
  --rm \
  -it \
  -u "node" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app/img-viewer" \
  node \
    yarn build
```


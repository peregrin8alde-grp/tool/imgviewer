# ドキュメント

## ビルド

```
find . -name "index.adoc" | sed -e "s|index.adoc||g" | xargs -I{} \
  docker run \
    -u 1000:1000 \
    --rm \
    -v $(pwd):/documents/ \
    asciidoctor/docker-asciidoctor \
      asciidoctor \
        -r asciidoctor-diagram \
        -D build/{} \
        {}index.adoc
```
